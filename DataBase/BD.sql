DROP DATABASE Verificador;
CREATE DATABASE sistema_verificador;
USE sistema_verificador;

CREATE TABLE Productos(
id_producto INT UNSIGNED NOT NULL UNIQUE,
nombre_producto VARCHAR(100) NOT NULL,
precio_producto FLOAT(12,2) UNSIGNED NOT NULL,
imagen_producto VARCHAR(255)
);

INSERT INTO Productos() Values(1,   "PlayStation 5",    9755.62,    "01Ps5.png");
INSERT INTO Productos() Values(2,   "Xbox One",         9384.62,    "02Xbox.png");
INSERT INTO Productos() Values(3,   "Switch",           6451.61,    "03Switch.png");
INSERT INTO Productos() Values(4,   "Steam Deck",       7800.59,    "04SteamDeck.png");
INSERT INTO Productos() Values(5,   "Atari",            2150.54,    "05Atari.png");
INSERT INTO Productos() Values(6,   "Nintendo 3DS",     3150.54,    "063DS.png");
INSERT INTO Productos() Values(7,   "PSP",              4150.54,    "07PSP.png");
INSERT INTO Productos() Values(8,   "PSP Vita",         1150.54,    "08PSPV.png");
INSERT INTO Productos() Values(9,   "Wii U",            150.54,     "09WiiU.png");
INSERT INTO Productos() Values(10,  "Game Cube",        8150.54,    "10GC.png");